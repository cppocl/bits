# Binary Numbers

Whole decimal numbers or Base 10 numbers is the commonly known system for representing numbers, such as 5, 18, 246, etc.

Binary numbers can represent whole decimal numbers using 0 or 1.

## Base 10

Base 10 uses columns for numbers 0 to 9, 10 to 99, 100 to 999, etc.

Multiplying each column by the unit size and number of units, then adding them together will produce the number.

Example of 246 split into columns for base 10.

| 100 | 10  |  1  |
|:---:|:---:|:---:|
|  2  |  4  |  6  |

(2 * 100) + (4 * 10) + 6 = 246

## Base 2

Base 2 uses columns for 2, 4, 8, 16, etc.

| 128 | 64  | 32  | 16  |  8  |  4  |  2  |  1  |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|  1  |  1  |  1  |  1  |  0  |  1  |  1  |  0  |

2 + 4 + 16 + 32 + 64 + 128 = 246

## Most and Least Significant Bit

The column with the largest value is known as the most significant bit or MSB, which in the above example is 128.

The column with the smallest value is known as the least significant bit or LSB, which is always the column containing 1.
