/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BITS_INTERNAL_INTERNALEXPANDBITS_HPP
#define OCL_GUARD_BITS_INTERNAL_INTERNALEXPANDBITS_HPP

#include "InternalMapBits.hpp"
#include <cstdint>
#include <cstddef>

namespace ocl
{

// ExpandBits a smaller value into a larger value for setting values in a buffer.
template<typename Type, std::size_t const SizeOfType, std::size_t const ExpandSizeOfType>
class InternalExpandBits;

template<typename Type>
class InternalExpandBits<Type, 1U, 1U>
{
/// Types and constants.
public:
    typedef Type         type;
    typedef std::uint8_t unsigned_type;
    typedef std::uint8_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        return InternalMapBits<type, expand_type>::ToBits(value);
    }
};

template<typename Type>
class InternalExpandBits<Type, 1U, 2U>
{
/// Types and constants.
public:
    typedef Type          type;
    typedef std::uint8_t  unsigned_type;
    typedef std::uint16_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        expand_type expanded_value = InternalMapBits<type, unsigned_type>::ToBits(value);
        expanded_value |= expanded_value << 8U;
        return expanded_value;
    }
};

template<typename Type>
class InternalExpandBits<Type, 2U, 2U>
{
/// Types and constants.
public:
    typedef Type          type;
    typedef std::uint16_t unsigned_type;
    typedef std::uint16_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        return InternalMapBits<type, unsigned_type>::ToBits(value);
    }
};

template<typename Type>
class InternalExpandBits<Type, 1U, 4U>
{
/// Types and constants.
public:
    typedef Type          type;
    typedef std::uint8_t  unsigned_type;
    typedef std::uint32_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        expand_type expanded_value = InternalMapBits<type, unsigned_type>::ToBits(value);
        expanded_value |= expanded_value << 8U;
        expanded_value |= expanded_value << 16U;
        return expanded_value;
    }
};

template<typename Type>
class InternalExpandBits<Type, 2U, 4U>
{
/// Types and constants.
public:
    typedef Type          type;
    typedef std::uint16_t unsigned_type;
    typedef std::uint32_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        expand_type expanded_value = InternalMapBits<type, unsigned_type>::ToBits(value);
        expanded_value |= expanded_value << 16U;
        return expanded_value;
    }
};

template<typename Type>
class InternalExpandBits<Type, 4U, 4U>
{
/// Types and constants.
public:
    typedef Type          type;
    typedef std::uint32_t unsigned_type;
    typedef std::uint32_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        return InternalMapBits<type, unsigned_type>::ToBits(value);
    }
};

template<typename Type>
class InternalExpandBits<Type, 1U, 8U>
{
/// Types and constants.
public:
    typedef Type          type;
    typedef std::uint8_t  unsigned_type;
    typedef std::uint64_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        expand_type expanded_value = InternalMapBits<type, unsigned_type>::ToBits(value);
        expanded_value |= expanded_value << 8U;
        expanded_value |= expanded_value << 16U;
        return expanded_value | (expanded_value << 32U);
    }
};

template<typename Type>
class InternalExpandBits<Type, 2U, 8U>
{
/// Types and constants.
public:
    typedef Type          type;
    typedef std::uint16_t unsigned_type;
    typedef std::uint64_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        expand_type expanded_value = InternalMapBits<type, unsigned_type>::ToBits(value);
        expanded_value |= expanded_value << 16U;
        return expanded_value | (expanded_value << 32U);
    }
};

template<typename Type>
class InternalExpandBits<Type, 4U, 8U>
{
/// Types and constants.
public:
    typedef Type          type;
    typedef std::uint32_t unsigned_type;
    typedef std::uint64_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        expand_type expanded_value = InternalMapBits<type, unsigned_type>::ToBits(value);
        return expanded_value | (expanded_value << 32U);
    }
};

template<typename Type>
class InternalExpandBits<Type, 8U, 8U>
{
/// Types and constants.
public:
    typedef Type          type;
    typedef std::uint64_t unsigned_type;
    typedef std::uint64_t expand_type;

    static size_t const size_of_type        = sizeof(Type);
    static size_t const size_of_expand_type = sizeof(expand_type);

// Member functions.
public:
    static inline expand_type ExpandBits(type value) noexcept
    {
        return InternalMapBits<type, unsigned_type>::ToBits(value);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BITS_INTERNAL_INTERNALEXPANDBITS_HPP
