/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalCountBits.hpp"
#include <cstring>
#include <cstddef>
#include <cstdint>
#include <limits>

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

using ocl::InternalCountBits;

namespace
{
    template<typename T>
    static std::size_t CountSetBits(T value) throw()
    {
        std::size_t count = 0;
        while (value != 0)
        {
            if ((value & 1) != 0)
                ++count;
            value >>= 1;
        }
        return count;
    }

    template<typename T, typename UnsignedT>
    static bool TestSetRange(T step = 1)
    {
        typedef InternalCountBits<T> count_bits_type;

        T min_value = std::numeric_limits<T>::min();
        T max_value = std::numeric_limits<T>::max();
        for (T value = min_value; value < max_value; value += step)
        {
            if (count_bits_type::CountSetBits(value) != CountSetBits(static_cast<UnsignedT>(value)))
                return false;
            if (max_value - value < step)
                break;
        }
        return count_bits_type::CountSetBits(max_value) == CountSetBits(static_cast<UnsignedT>(max_value));
    }

    template<typename T, typename UnsignedT>
    static bool TestClearedRange(T step = 1)
    {
        typedef InternalCountBits<T> count_bits_type;

        T min_value = std::numeric_limits<T>::min();
        T max_value = std::numeric_limits<T>::max();
        for (T value = min_value; value < max_value; value += step)
        {
            if (count_bits_type::CountClearedBits(value) != (8U * sizeof(T)) - CountSetBits(static_cast<UnsignedT>(value)))
                return false;
            if (max_value - value < step)
                break;
        }
        return count_bits_type::CountClearedBits(max_value) == (8U * sizeof(T)) - CountSetBits(static_cast<UnsignedT>(max_value));
    }
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountSetBits, int8_t)
{
    bool success = TestSetRange<std::int8_t, std::uint8_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountSetBits, uint8_t)
{
    bool success = TestSetRange<std::uint8_t, std::uint8_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountSetBits, int16_t)
{
    bool success = TestSetRange<std::int16_t, std::uint16_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountSetBits, uint16_t)
{
    bool success = TestSetRange<std::uint16_t, std::uint16_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountSetBits, int32_t)
{
    bool success = TestSetRange<std::int32_t, std::uint32_t>(1000);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountSetBits, uint32_t)
{
    bool success = TestSetRange<std::uint32_t, std::uint32_t>(1000U);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountSetBits, int64_t)
{
    bool success = TestSetRange<std::int64_t, std::uint64_t>(10000000000000LL);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountSetBits, uint64_t)
{
    bool success = TestSetRange<std::uint64_t, std::uint64_t>(10000000000000ULL);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountClearedBits, int8_t)
{
    bool success = TestClearedRange<std::int8_t, std::uint8_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountClearedBits, uint8_t)
{
    bool success = TestClearedRange<std::uint8_t, std::uint8_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountClearedBits, int16_t)
{
    bool success = TestClearedRange<std::int16_t, std::uint16_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountClearedBits, uint16_t)
{
    bool success = TestClearedRange<std::uint16_t, std::uint16_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountClearedBits, int32_t)
{
    bool success = TestClearedRange<std::int32_t, std::uint32_t>(1000);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountClearedBits, uint32_t)
{
    bool success = TestClearedRange<std::uint32_t, std::uint32_t>(1000U);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountClearedBits, int64_t)
{
    bool success = TestClearedRange<std::int64_t, std::uint64_t>(10000000000000LL);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(InternalCountBits, CountClearedBits, uint64_t)
{
    bool success = TestClearedRange<std::uint64_t, std::uint64_t>(10000000000000ULL);
    CHECK_TRUE(success);
}
