/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalCountByteBits.hpp"
#include <cstring>
#include <cstddef>
#include <cstdint>
#include <limits>

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

namespace
{
    template<typename T>
    static std::size_t CountSetBits(T value) noexcept
    {
        std::size_t count = 0;
        while (value > 0)
        {
            if ((value & 1) != 0)
                ++count;
            value >>= 1;
        }
        return count;
    }

    template<typename T>
    static bool TestSetRange()
    {
        typedef InternalCountByteBits<std::size_t> count_byte_bits_type;

        T max_value = std::numeric_limits<T>::max();
        for (T value = 0; value < max_value; ++value)
            if (count_byte_bits_type::CountSetBits(value) != CountSetBits(value))
                return false;
        return count_byte_bits_type::CountSetBits(max_value) == CountSetBits(max_value);
    }

    template<typename T>
    static bool TestClearedRange()
    {
        typedef InternalCountByteBits<std::size_t> count_byte_bits_type;

        T max_value = std::numeric_limits<T>::max();
        for (T value = 0; value < max_value; ++value)
            if (count_byte_bits_type::CountClearedBits(value) != 8U - CountSetBits(value))
                return false;
        return count_byte_bits_type::CountClearedBits(max_value) == 8U - CountSetBits(max_value);
    }
}

using ocl::InternalCountByteBits;

TEST_MEMBER_FUNCTION(InternalCountByteBits, CountSetBits, uint8_t)
{
    typedef InternalCountByteBits<std::size_t> count_byte_bits_type;

    CHECK_TRUE(TestSetRange<std::uint8_t>());
}

TEST_MEMBER_FUNCTION(InternalCountByteBits, CountClearedBits, uint8_t)
{
    typedef InternalCountByteBits<std::size_t> count_byte_bits_type;

    CHECK_TRUE(TestClearedRange<std::uint8_t>());
}
