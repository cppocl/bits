/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalExpandBits.hpp"
#include <cstring>
#include <cstddef>
#include <cstdint>

using ocl::InternalExpandBits;

#define UINT8_TO_UINT16(value)  (static_cast<uint16_t>(static_cast<uint8_t>(value)) | \
                                (static_cast<uint16_t>(static_cast<uint8_t>(value)) << 8U))

#define UINT8_TO_UINT32(value)  (static_cast<uint32_t>(static_cast<uint8_t>(value)) | \
                                (static_cast<uint32_t>(static_cast<uint8_t>(value)) << 8U) | \
                                (static_cast<uint32_t>(static_cast<uint8_t>(value)) << 16U) | \
                                (static_cast<uint32_t>(static_cast<uint8_t>(value)) << 24))

#define UINT16_TO_UINT32(value) (static_cast<uint32_t>(static_cast<uint16_t>(value)) | \
                                (static_cast<uint32_t>(static_cast<uint16_t>(value)) << 16U))

#define UINT8_TO_UINT64(value)  (static_cast<uint64_t>(static_cast<uint8_t>(value)) | \
                                (static_cast<uint64_t>(static_cast<uint8_t>(value)) << 8U) | \
                                (static_cast<uint64_t>(static_cast<uint8_t>(value)) << 16U) | \
                                (static_cast<uint64_t>(static_cast<uint8_t>(value)) << 24U) | \
                                (static_cast<uint64_t>(static_cast<uint8_t>(value)) << 32U) | \
                                (static_cast<uint64_t>(static_cast<uint8_t>(value)) << 40U) | \
                                (static_cast<uint64_t>(static_cast<uint8_t>(value)) << 48U) | \
                                (static_cast<uint64_t>(static_cast<uint8_t>(value)) << 56))

#define UINT16_TO_UINT64(value) (static_cast<uint64_t>(static_cast<uint16_t>(value)) | \
                                (static_cast<uint64_t>(static_cast<uint16_t>(value)) << 16U) | \
                                (static_cast<uint64_t>(static_cast<uint16_t>(value)) << 32U) | \
                                (static_cast<uint64_t>(static_cast<uint16_t>(value)) << 48U))

#define UINT32_TO_UINT64(value) (static_cast<uint64_t>(static_cast<uint32_t>(value)) | \
                                (static_cast<uint64_t>(static_cast<uint32_t>(value)) << 32U))

TEST_MEMBER_FUNCTION(InternalExpandBits, ExpandBits, char)
{
    typedef char type;
    typedef unsigned char unsigned_type;

    // Test that a char can be expanded into 16, 32 and 64 bits.

    {
        typedef InternalExpandBits<type, sizeof(type), 2U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(127);
        expand_type expected_value = UINT8_TO_UINT16(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 2U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(-128);
        expand_type expected_value = UINT8_TO_UINT16(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 2U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x38);
        expand_type expected_value = UINT8_TO_UINT16(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(127);
        expand_type expected_value = UINT8_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(-128);
        expand_type expected_value = UINT8_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x38);
        expand_type expected_value = UINT8_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(127);
        expand_type expected_value = UINT8_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(-128);
        expand_type expected_value = UINT8_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x38);
        expand_type expected_value = UINT8_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }
}

TEST_MEMBER_FUNCTION(InternalExpandBits, ExpandBits, signed_char)
{
    typedef signed char type;
    typedef unsigned char unsigned_type;

    TEST_OVERRIDE_ARGS("signed char");

    // Test that a signed char can be expanded into 16, 32 and 64 bits.

    {
        typedef InternalExpandBits<type, sizeof(type), 2U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(127);
        expand_type expected_value = UINT8_TO_UINT16(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 2U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(-128);
        expand_type expected_value = UINT8_TO_UINT16(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 2U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x38);
        expand_type expected_value = UINT8_TO_UINT16(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(127);
        expand_type expected_value = UINT8_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(-128);
        expand_type expected_value = UINT8_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x38);
        expand_type expected_value = UINT8_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(127);
        expand_type expected_value = UINT8_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(-128);
        expand_type expected_value = UINT8_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x38);
        expand_type expected_value = UINT8_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }
}

TEST_MEMBER_FUNCTION(InternalExpandBits, ExpandBits, unsigned_char)
{
    typedef unsigned char type;
    typedef unsigned char unsigned_type;

    TEST_OVERRIDE_ARGS("unsigned char");

    // Test that a unsigned char can be expanded into 16, 32 and 64 bits.

    {
        typedef InternalExpandBits<type, sizeof(type), 2U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(255);
        expand_type expected_value = UINT8_TO_UINT16(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 2U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0);
        expand_type expected_value = UINT8_TO_UINT16(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 2U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x38);
        expand_type expected_value = UINT8_TO_UINT16(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(255);
        expand_type expected_value = UINT8_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0);
        expand_type expected_value = UINT8_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x38);
        expand_type expected_value = UINT8_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(255);
        expand_type expected_value = UINT8_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0);
        expand_type expected_value = UINT8_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x38);
        expand_type expected_value = UINT8_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }
}

TEST_MEMBER_FUNCTION(InternalExpandBits, ExpandBits, int16_t)
{
    typedef int16_t type;
    typedef uint16_t unsigned_type;

    // Test that a int16_t can be expanded into 32 and 64 bits.

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x7fff);
        expand_type expected_value = UINT16_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(-0x7fff - 1);
        expand_type expected_value = UINT16_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x3816);
        expand_type expected_value = UINT16_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x7fff);
        expand_type expected_value = UINT16_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(-0x7fff - 1);
        expand_type expected_value = UINT16_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x3816);
        expand_type expected_value = UINT16_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }
}

TEST_MEMBER_FUNCTION(InternalExpandBits, ExpandBits, uint16_t)
{
    typedef uint16_t type;
    typedef uint16_t unsigned_type;

    // Test that a uint16_t can be expanded into 32 and 64 bits.

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0xffff);
        expand_type expected_value = UINT16_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0);
        expand_type expected_value = UINT16_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 4U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x8316);
        expand_type expected_value = UINT16_TO_UINT32(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0xffff);
        expand_type expected_value = UINT16_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0);
        expand_type expected_value = UINT16_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x8316);
        expand_type expected_value = UINT16_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }
}

TEST_MEMBER_FUNCTION(InternalExpandBits, ExpandBits, int32_t)
{
    typedef int32_t type;
    typedef uint16_t unsigned_type;

    // Test that a int32_t can be expanded into 64 bits.

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x7fffffff);
        expand_type expected_value = UINT32_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(-0x7fffffff - 1);
        expand_type expected_value = UINT32_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x381a234c);
        expand_type expected_value = UINT32_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }
}

TEST_MEMBER_FUNCTION(InternalExpandBits, ExpandBits, uint32_t)
{
    typedef uint32_t type;
    typedef uint16_t unsigned_type;

    // Test that a uint32_t can be expanded into 64 bits.

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0xffffffff);
        expand_type expected_value = UINT32_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0);
        expand_type expected_value = UINT32_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }

    {
        typedef InternalExpandBits<type, sizeof(type), 8U> expand_value_type;
        typedef expand_value_type::expand_type expand_type;
        type const value = static_cast<type>(0x831a234c);
        expand_type expected_value = UINT32_TO_UINT64(value);
        CHECK_EQUAL(expand_value_type::ExpandBits(value), expected_value);
        CHECK_EQUAL(static_cast<type>(expected_value), value);
    }
}
